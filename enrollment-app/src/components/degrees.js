import React from 'react';
import { Button } from 'react-bootstrap';
const axios = require('axios').default;

const Degrees = ({ degrees, user }) => {
    return (
        <div>
            <center>
                <h1>Degrees List</h1>
                <h5> Logged in as {user.name}, key: {user.key} </h5>
            </center>
            {degrees.map(function (degree) {
                if (degree.available) {

                    return (
                        <div className="card" key={degree.key}>

                            <div className="card-body">
                                <h5 className="card-title">{degree.title}</h5>
                                <h6 className="card-subtitle mb-2 text-muted">{degree.slug}</h6>
                                <p className="card-text">{degree.short_summary}</p>

                                <Button id={degree.key} type="submit" onClick={async () => {
                                    /**
                                     * enhancement counter if the user already enrolled by send to backend using filters, 
                                     * but filters now is not implemented from the backend side same as Udacity catalogue URL
                                     */
                                    await axios.post("http://127.0.0.1:8080/v1/enrollment/",
                                        {
                                            NanodegreeKey: degree.key,
                                            UdacityUserKey: user.key,
                                            Status: "ENROLLED"

                                        }
                                    ).then(function (response) {
                                        console.log(response.status);
                                    }).catch(function (error) {
                                        alert(error)
                                    });
                                }}>Enroll</Button>
                            </div>
                            {/* <img className="card-img-bottom" src={degree.image} /> */}
                        </div>
                    );
                }
                return;
            }
            )
            }
        </div>
    )
};

export default Degrees