import './App.css';
import Degrees from './components/degrees';
import React, { Component } from 'react';

class App extends Component {
  state = {
    degrees: [],
    user: "",
  }
  componentDidMount() {
    fetch('https://catalog-api.udacity.com/v1/degrees?locale=en-us')
      .then(res => res.json())
      .then((data) => {
        this.setState({
          degrees: data.degrees,
          user: {
            key: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),
            name: "George Messiha"
          }
        })
      })
      .catch(console.log);

  }
  render() {
    return (
      <div>
        <Degrees degrees={this.state.degrees} user={this.state.user}></Degrees>
      </div>
    );
  }
}

export default App;