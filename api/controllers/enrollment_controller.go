package controllers

import (
	"net/http"
	"strconv"
	"gitlab.com/georgemessiha22/api/models"
	"github.com/labstack/echo/v4"
	_ "github.com/swaggo/swag/example/celler/httputil"
)

// GetEnrollments godoc
// @Summary Show all Enrollments
// @Description get Enrollments
// @ID get-enrollments
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Enrollment
// @Failure 400 {object} httputil.HTTPError
// @Failure 401 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /enrollments/ [get]
func (server *Server) GetEnrollments(c echo.Context) error {
	enrollment := models.Enrollment{}

	// read all enrollments from database
	enrollments, err := enrollment.FindAllEnrollments(server.DB)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}
	return c.JSON(http.StatusOK, enrollments)
}

// GetEnrollment godoc
// @Summary Show a Enrollment
// @Description get Enrollment by ID
// @ID get-enrollment-by-int
// @Accept  json
// @Produce  json
// @Param id path int true "Enrollment ID"
// @Success 200 {object} models.Enrollment
// @Failure 400 {object} httputil.HTTPError
// @Failure 401 {object} httputil.HTTPError
// @Failure 500 {object} httputil.HTTPError
// @Router /enrollment/{id} [get]
func (server *Server) GetEnrollment(c echo.Context) error {
	// read id from the link
	id, _ := strconv.ParseUint(c.Param("id"), 0, 64)
	enrollment := new(models.Enrollment)
	e, err := enrollment.FindEnrollmentByID(server.DB, id)
	if err != nil {
		return c.JSON(404, err)
	}
	return c.JSON(http.StatusOK, e)
}

// CreateEnrollment godoc
// @Summary Create new Enrollment
// @Description Create new Enrollments
// @ID set-enrollments
// @Accept  json
// @Produce  json
// @Param Enrollment body models.EnrollmentSave true "Enrollment data"
// @Success 201 {object} models.Enrollment
// @Failure 500 {object} httputil.HTTPError
// @Router /enrollment/ [post]
func (server *Server) CreateEnrollment(c echo.Context) error {
	e_save := new(models.EnrollmentSave)

	// translate the POST input
	if err := c.Bind(e_save); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}
	e := new(models.Enrollment)
	e.NanodegreeKey = e_save.NanodegreeKey
	e.UdacityUserKey = e_save.UdacityUserKey
	e.Status = e_save.Status

	// run validation 
	if err := e.Validate(); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}

	// save the new enrollment
	e, err := e.SaveEnrollment(server.DB)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}
	return c.JSON(http.StatusCreated, e)
}

// UpdateEnrollment godoc
// @Summary Update a Enrollments
// @Description update Enrollments
// @ID update-enrollments
// @Accept  json
// @Produce  json
// @Param id path int true "Enrollment ID"
// @Param Enrollment body models.EnrollmentSave true "Enrollment data"
// @Success 200 {object} models.Enrollment
// @Failure 500 {object} httputil.HTTPError
// @Router /enrollment/{id} [put]
func (server *Server) UpdateEnrollment(c echo.Context) error {
	// read id from the link
	id, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	e_save := new(models.EnrollmentSave)
	if err := c.Bind(e_save); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}
	e := new(models.Enrollment)
	e.NanodegreeKey = e_save.NanodegreeKey
	e.UdacityUserKey = e_save.UdacityUserKey
	e.Status = e_save.Status
	e.ID = id

	if err:= e.Validate(); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}
	e, err := e.UpdateAEnrollment(server.DB, id)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}
	return c.JSON(http.StatusOK, e)
}

// DeleteEnrollment godoc
// @Summary Delete a Enrollments
// @Description delete Enrollments
// @ID delete-enrollments
// @Accept  json
// @Produce  json
// @Param id path int true "Enrollment ID"
// @Success 200 {int} models.Enrollment
// @Failure 500 {object} httputil.HTTPError
// @Router /enrollment/ [delete]
func (server *Server) DeleteEnrollment(c echo.Context) error {
	// read id from the link
	id, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	e := new(models.Enrollment)
	if err := c.Bind(e); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}
	_, err := e.DeleteEnrollment(server.DB, id)
	if err != nil {
		return c.JSON(http.StatusUnprocessableEntity, err)
	}
	return c.JSON(http.StatusNoContent, nil)
}
