package controllers

import (
	"github.com/swaggo/echo-swagger"

	_ "gitlab.com/georgemessiha22/api/docs"
)

// @title Enrollment API
// @version 1.0
// @description This is a sample server for Udacity enrollment microservice.

// @contact.name API Developer
// @contact.url http://www.linkedin.com/in/georgemessiha22
// @contact.email georgemessiha22@gmail.com

// @license.name GNU GENERAL PUBLIC LICENSE
// @license.url https://www.gnu.org/licenses/gpl-3.0.en.html

// @host 127.0.0.1:8080
// @BasePath /v1/
func (server *Server) initializeRoutes() {
	server.Echo.GET("/v1/enrollments/", server.GetEnrollments)
	server.Echo.GET("/v1/enrollment/:id/", server.GetEnrollment)
	server.Echo.POST("/v1/enrollment/", server.CreateEnrollment)
	server.Echo.PUT("/v1/enrollment/:id/", server.UpdateEnrollment)
	server.Echo.DELETE("/v1/enrollment/:id/", server.DeleteEnrollment)

	server.Echo.GET("/swagger/*any", echoSwagger.WrapHandler)
}