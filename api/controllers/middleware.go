package controllers

import (
	"net/http"
	"github.com/labstack/echo/v4/middleware"
)

func (server *Server) initializeMiddleware() {
	// Middleware
	server.Echo.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:3000", "http://127.0.0.1:3000", "http://127.0.0.1:8080"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	server.Echo.Use(middleware.Logger())
	server.Echo.Use(middleware.Recover())
}