package controllers_test

import (
	// "fmt"
	"encoding/json"
	"os"
	"gitlab.com/georgemessiha22/api/controllers"
	"gitlab.com/georgemessiha22/api/models"
	"log"
	"strconv"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

var ServerTest = controllers.Server{}
var enrollmentInstance = models.Enrollment{}

func refreshEnrollmentTable() error {
	DB_HOST := os.Getenv("DB_HOST")
	DB_USER := os.Getenv("DB_USER")
	DB_PASSWORD := os.Getenv("DB_PASSWORD")
	DB_NAME := os.Getenv("DB_NAME")
	DB_PORT := os.Getenv("DB_PORT")
	ServerTest.Initialize(DB_USER, DB_PASSWORD, DB_PORT, DB_HOST, DB_NAME)
	err := ServerTest.DB.DropTableIfExists(&models.Enrollment{}).Error
	if err != nil {
		return err
	}
	err = ServerTest.DB.AutoMigrate(&models.Enrollment{}).Error
	if err != nil {
		return err
	}
	return nil
}

func seedOneEnrollment() (models.Enrollment, error) {

	refreshEnrollmentTable()

	e := models.Enrollment{
		NanodegreeKey: "123132131312",
		UdacityUserKey:    "2132313123131",
		Status: "Enrolled",
	}

	err := ServerTest.DB.Model(&models.Enrollment{}).Create(&e).Error
	if err != nil {
		log.Fatalf("cannot seed enrollment table: %v", err)
	}
	return e, nil
}

func seedEnrollments() ([]models.Enrollment, error) {

	es := []models.Enrollment{
		models.Enrollment{
			NanodegreeKey: "123132131312",
		UdacityUserKey:    "2132313123131",
		Status: "Enrolled",
		},
		models.Enrollment{
			NanodegreeKey: "1231321313124",
		UdacityUserKey:    "21323131231314",
		Status: "UnEnrolled",
		},
	}

	for i, _ := range es {
		err := ServerTest.DB.Model(&models.Enrollment{}).Create(&es[i]).Error
		if err != nil {
			return nil, err
		}
	}
	return es, nil
}

func TestCreateEnrollment(t *testing.T) {
	err := refreshEnrollmentTable()
	if err != nil {
		log.Fatal(err)
	}
	samples := []struct {
		inputJSON    string
		statusCode   int
		NanodegreeKey     string
		UdacityUserKey        string
		Status string
		errorMessage string
	}{
		{
			inputJSON:    `{
				"NanodegreeKey": "123132131312",
				"UdacityUserKey":    "2132313123131",
				"Status": "ENROLLED"
			}`,
			statusCode:   201,
			NanodegreeKey:     "123132131312",
			UdacityUserKey:        "2132313123131",
			Status: "ENROLLED",
			errorMessage: "",
		},
		{
			inputJSON:    `{
				"NanodegreeKey": "123132131312",
				"UdacityUserKey":    "2132313123131",
				"Status": "blabla"
			}`,
			statusCode:   422,
			errorMessage: "Enrollment  status either ‘ENROLLED’ or ‘UNENROLLED’",
		},
		{
			inputJSON:    `{
				"NanodegreeKey": "",
				"UdacityUserKey":    "2132313123131",
				"Status": "Enrolled"
			}`,
			statusCode:   422,
			errorMessage: "Required NanodegreeKey",
		},
		{
			inputJSON:    `{
				"NanodegreeKey": "123132131312",
				"UdacityUserKey":    "",
				"Status": "Enrolled"
			}`,
			statusCode:   422,
			errorMessage: "Required UdacityUserKey",
		},
	}

	for _, v := range samples {

		req := httptest.NewRequest(http.MethodPost, "/v1/enrollment/", strings.NewReader(v.inputJSON))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		rec := httptest.NewRecorder()
		c := ServerTest.Echo.NewContext(req, rec)

		if v.statusCode == 201 {
			if assert.NoError(t, ServerTest.CreateEnrollment(c)) {
				e := new(models.Enrollment)
				err = json.Unmarshal([]byte(rec.Body.String()), &e)
				if err != nil {
					log.Fatalf("Cannot convert to json: %v\n", err)
				}
				assert.Equal(t, http.StatusCreated, rec.Code)
				assert.Equal(t, v.NanodegreeKey, e.NanodegreeKey)
				assert.Equal(t, v.UdacityUserKey, e.UdacityUserKey)
				assert.Equal(t, v.Status, e.Status)
			}
			}
		if v.statusCode == 422 && v.errorMessage != "" {
			if assert.NoError(t, ServerTest.CreateEnrollment(c)) {
				assert.Equal(t, 422, rec.Code)
			}
		}
		if v.statusCode == 500 && v.errorMessage != "" {
			if assert.NoError(t, ServerTest.CreateEnrollment(c)) {
				assert.Equal(t, 500, rec.Code)
			}
		}
	}
}

func TestGetEnrollments(t *testing.T) {

	err := refreshEnrollmentTable()
	if err != nil {
		log.Fatal(err)
	}
	_, err = seedEnrollments()
	if err != nil {
		log.Fatal(err)
	}
	request := httptest.NewRequest("GET", "/v1/enrollments/", nil)

	request.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	recorder := httptest.NewRecorder()
	context := ServerTest.Echo.NewContext(request, recorder)

	var es []models.Enrollment
	if assert.NoError(t, ServerTest.GetEnrollments(context)) {
		if err := json.Unmarshal(recorder.Body.Bytes(), &es);err != nil {
			log.Fatalf("Cannot convert to json: %v\n", err)
		}
		assert.Equal(t, http.StatusOK, recorder.Code)
		assert.Equal(t, len(es), 2)
	}
	
}

func TestGetEnrollmentByID(t *testing.T) {
	if err := refreshEnrollmentTable(); err != nil {
		log.Fatal(err)
	}
	e, err := seedOneEnrollment()
	if err != nil {
		log.Fatal(err)
	}

	enrollmentSample := []struct {
		id           string
		statusCode   int
		NanodegreeKey     string
		UdacityUserKey        string
		Status string
		errorMessage string
	}{
		{
			id:  strconv.FormatUint(e.ID, 10),
			statusCode: 200,
			NanodegreeKey:   e.NanodegreeKey,
			UdacityUserKey:      e.UdacityUserKey,
			Status: e.Status,
		},
		{
			id:         "unknwon",
			statusCode: 404,
		},
	}

	for _, v := range enrollmentSample {
		request := httptest.NewRequest("GET", "/" , nil)
		request.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		recorder := httptest.NewRecorder()
		context := ServerTest.Echo.NewContext(request, recorder)
		context.SetPath("/v1/enrollment/:id")
		context.SetParamNames("id")
		context.SetParamValues(v.id)

		var responseMap = make(map[string]interface{})
		if assert.NoError(t, ServerTest.GetEnrollment(context)) {
			if err := json.Unmarshal(recorder.Body.Bytes(), &responseMap); err != nil {
				log.Fatalf("Cannot convert to json: %v", err)
			}
			assert.Equal(t, v.statusCode, recorder.Code)
			if (v.statusCode == http.StatusOK) && (recorder.Code == http.StatusOK) {
				assert.Equal(t, e.NanodegreeKey, responseMap["NanodegreeKey"])
				assert.Equal(t, e.UdacityUserKey, responseMap["UdacityUserKey"])
				assert.Equal(t, e.Status, responseMap["Status"])
			}
		}
	}
}

func TestUpdateEnrollment(t *testing.T) {
	var EnrollmentID uint64

	err := refreshEnrollmentTable()
	if err != nil {
		log.Fatal(err)
	}
	es, err := seedEnrollments()
	if err != nil {
		log.Fatalf("Error seeding enrollment: %v\n", err)
	}
	for _, e := range es {
		if e.ID == 2 {
			continue
		}
		EnrollmentID = e.ID
	}
	
	samples := []struct {
		id             string
		updateJSON     string
		statusCode     int
		updateNanodegreeKey string
		updateUdacityUserKey    string
		updateStatus string
		errorMessage   string
	}{
		{
			id:           strconv.FormatUint(EnrollmentID, 10),
			updateJSON:     `{
				"NanodegreeKey": "1231321313dd12",
				"UdacityUserKey":    "2132313123131",
				"Status": "ENROLLED"
			}`,
			statusCode:     200,
			updateNanodegreeKey:"1231321313dd12",
			updateUdacityUserKey:"2132313123131",
			updateStatus:"ENROLLED",
			errorMessage:   "",
		},
		{
			id:   strconv.FormatUint(EnrollmentID, 10),
			updateJSON:     `{
				"NanodegreeKey": "1231321313dd12",
				"UdacityUserKey":    "2132313123131",
				"Status": "invalid"
			}`,
			statusCode:     422,
			updateNanodegreeKey:"1231321313dd12",
			updateUdacityUserKey:"2132313123131",
			updateStatus:"invalid",
			errorMessage: "Enrollment status either ‘ENROLLED’ or ‘UNENROLLED’",
		},
	}

	for _, v := range samples {
		request := httptest.NewRequest("PUT", "/" , strings.NewReader(v.updateJSON))
		request.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		recorder := httptest.NewRecorder()
		context := ServerTest.Echo.NewContext(request, recorder)
		context.SetPath("/v1/enrollment/:id/")
		context.SetParamNames("id")
		context.SetParamValues(v.id)
		

		responseMap := make(map[string]interface{})
		if assert.NoError(t, ServerTest.UpdateEnrollment(context)) {
			if err := json.Unmarshal(recorder.Body.Bytes(), &responseMap); err != nil {
				t.Errorf(err.Error())
			}
			assert.Equal(t,  v.statusCode, recorder.Code)

			if v.statusCode == 200  && recorder.Code == 200{
				assert.Equal(t, v.updateNanodegreeKey, responseMap["NanodegreeKey"])
				assert.Equal(t, v.updateUdacityUserKey, responseMap["UdacityUserKey"])
				assert.Equal(t, v.updateStatus, responseMap["Status"])
			}
		}
	}
}

func TestDeleteEnrollment(t *testing.T) {

	var EnrollmentID uint64

	err := refreshEnrollmentTable()
	if err != nil {
		log.Fatal(err)
	}

	es, err := seedEnrollments()
	if err != nil {
		log.Fatalf("Error seeding enrollment: %v\n", err)
	}

	for _, e := range es {
		if e.ID == 2 {
			continue
		}
		EnrollmentID = e.ID
	}

	enrollmentSample := []struct {
		id           string
		statusCode   int
		errorMessage string
	}{
		{
			id:           strconv.FormatUint(EnrollmentID, 10),
			statusCode:   204,
			errorMessage: "",
		},
		{
			id:         "unknwon",
			statusCode: 422,
		},
	}
	for _, v := range enrollmentSample {

		request := httptest.NewRequest("DELETE", "/" , nil)
		request.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		recorder := httptest.NewRecorder()
		context := ServerTest.Echo.NewContext(request, recorder)
		context.SetPath("/v1/enrollment/:id/")
		context.SetParamNames("id")
		context.SetParamValues(v.id)

		responseMap := make(map[string]interface{})
		if assert.NoError(t, ServerTest.DeleteEnrollment(context)) {
			if err := json.Unmarshal(recorder.Body.Bytes(), &responseMap); err != nil {
				t.Errorf(err.Error())
			}
			assert.Equal(t,  v.statusCode, recorder.Code)
		}
	}
}
