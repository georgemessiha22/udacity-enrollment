package controllers

import (
	"fmt"
	"log"

	"gitlab.com/georgemessiha22/api/models"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/labstack/echo/v4"
)

type Server struct {
	DB   *gorm.DB
	Echo *echo.Echo
}

func (server *Server) Initialize(DbUser, DbPassword, DbPort, DbHost, DbName string) {

	var err error

	DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)
	server.DB, err = gorm.Open("postgres", DBURL)
	if err != nil {
		// fmt.Printf("Cannot connect to postgres database")
		log.Fatal("This is the error:", err)
	}

	server.DB.Debug().AutoMigrate(&models.Enrollment{}) //database migration

	server.Echo = echo.New()
	server.initializeMiddleware()
	server.initializeRoutes()
}

func (server *Server) Run(port string) {
	addr := ":" + port
	server.Echo.Logger.Fatal(server.Echo.Start(addr))
}
