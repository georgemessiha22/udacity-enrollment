package models_test

import (
	"log"
	"testing"
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	"gitlab.com/georgemessiha22/api/models"
	_ "github.com/jinzhu/gorm/dialects/postgres" //postgres driver
	"gopkg.in/go-playground/assert.v1"
)

var db *gorm.DB
var enrollmentInstance = models.Enrollment{}

func refreshEnrollmentTable() error {
	DB_HOST := os.Getenv("DB_HOST")
	DB_USER := os.Getenv("DB_USER")
	DB_PASSWORD := os.Getenv("DB_PASSWORD")
	DB_NAME := os.Getenv("DB_NAME")
	DB_PORT := os.Getenv("DB_PORT")
	DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", 
	DB_HOST, DB_PORT, DB_USER, DB_NAME, DB_PASSWORD)
	db, _ = gorm.Open("postgres", DBURL)
	err := db.DropTableIfExists(&models.Enrollment{}).Error
	if err != nil {
		return err
	}
	err = db.AutoMigrate(&models.Enrollment{}).Error
	if err != nil {
		return err
	}
	return nil
}

func seedOneEnrollment() (models.Enrollment, error) {

	refreshEnrollmentTable()

	e := models.Enrollment{
		NanodegreeKey: "123132131312",
		UdacityUserKey:    "2132313123131",
		Status: "Enrolled",
	}

	err := db.Model(&models.Enrollment{}).Create(&e).Error
	if err != nil {
		log.Fatalf("cannot seed enrollment table: %v", err)
	}
	return e, nil
}

func seedEnrollments() error {

	es := []models.Enrollment{
		models.Enrollment{
			NanodegreeKey: "123132131312",
		UdacityUserKey:    "2132313123131",
		Status: "Enrolled",
		},
		models.Enrollment{
			NanodegreeKey: "1231321313124",
		UdacityUserKey:    "21323131231314",
		Status: "UnEnrolled",
		},
	}

	for i, _ := range es {
		err := db.Model(&enrollmentInstance).Create(&es[i]).Error
		if err != nil {
			return err
		}
	}
	return nil
}

func TestFindAllEnrollments(t *testing.T) {

	err := refreshEnrollmentTable()
	if err != nil {
		log.Fatal(err)
	}

	err = seedEnrollments()
	if err != nil {
		log.Fatal(err)
	}

	es, err := enrollmentInstance.FindAllEnrollments(db)
	if err != nil {
		t.Errorf("this is the error getting the enrollments: %v\n", err)
		return
	}
	assert.Equal(t, len(*es), 2)
}

func TestSaveEnrollment(t *testing.T) {

	err := refreshEnrollmentTable()
	if err != nil {
		log.Fatal(err)
	}
	newEnrollment := models.Enrollment{
		NanodegreeKey: "123132131312",
		UdacityUserKey:    "2132313123131",
		Status: "Enrolled",
	}
	savedEnrollment, err := newEnrollment.SaveEnrollment(db)
	if err != nil {
		t.Errorf("this is the error getting the enrollments: %v\n", err)
		return
	}
	assert.Equal(t, newEnrollment.NanodegreeKey, savedEnrollment.NanodegreeKey)
	assert.Equal(t, newEnrollment.UdacityUserKey, savedEnrollment.UdacityUserKey)
	assert.Equal(t, newEnrollment.Status, savedEnrollment.Status)
}

func TestGetEnrollmentByID(t *testing.T) {

	err := refreshEnrollmentTable()
	if err != nil {
		log.Fatal(err)
	}

	e, err := seedOneEnrollment()
	if err != nil {
		log.Fatalf("cannot seed enrollments table: %v", err)
	}
	foundEnrollment, err := enrollmentInstance.FindEnrollmentByID(db, e.ID)
	if err != nil {
		t.Errorf("this is the error getting one enrollment: %v\n", err)
		return
	}
	assert.Equal(t, foundEnrollment.ID, e.ID)
	assert.Equal(t, foundEnrollment.NanodegreeKey, e.NanodegreeKey)
	assert.Equal(t, foundEnrollment.UdacityUserKey, e.UdacityUserKey)
	assert.Equal(t, foundEnrollment.Status, e.Status)
}

func TestUpdateAEnrollment(t *testing.T) {

	err := refreshEnrollmentTable()
	if err != nil {
		log.Fatal(err)
	}

	e, err := seedOneEnrollment()
	if err != nil {
		log.Fatalf("Cannot seed enrollment: %v\n", err)
	}

	enrollmentUpdate := models.Enrollment{
		NanodegreeKey: "123132131312",
		UdacityUserKey:    "2132313123131",
		Status: "Enrolled",
	}
	updatedEnrollment, err := enrollmentUpdate.UpdateAEnrollment(db, e.ID)
	if err != nil {
		t.Errorf("this is the error updating the enrollment: %v\n", err)
		return
	}
	assert.Equal(t, updatedEnrollment.ID, enrollmentUpdate.ID)
	assert.Equal(t, updatedEnrollment.NanodegreeKey, enrollmentUpdate.NanodegreeKey)
	assert.Equal(t, updatedEnrollment.UdacityUserKey, enrollmentUpdate.UdacityUserKey)
	assert.Equal(t, updatedEnrollment.Status, enrollmentUpdate.Status)
}

func TestDeleteEnrollment(t *testing.T) {

	err := refreshEnrollmentTable()
	if err != nil {
		log.Fatal(err)
	}

	e, err := seedOneEnrollment()

	if err != nil {
		log.Fatalf("Cannot seed enrollment: %v\n", err)
	}

	isDeleted, err := enrollmentInstance.DeleteEnrollment(db, e.ID)
	if err != nil {
		t.Errorf("this is the error updating the enrollment: %v\n", err)
		return
	}

	//Can be done this way too
	assert.Equal(t, isDeleted, int64(1))
}
