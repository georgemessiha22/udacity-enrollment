package models

import (
	"errors"
	"time"
	"github.com/jinzhu/gorm"
)

// Enrollment object
type Enrollment struct {
	ID             uint64   `gorm:"primary_key;auto_increment" json:"ID"`
	NanodegreeKey  string   `gorm:"unique_index:NanodegreeKey_UdacityUserKey_code;size:255;not null;" json:"NanodegreeKey"`
	UdacityUserKey string   `gorm:"unique_index:NanodegreeKey_UdacityUserKey_code;size:255;not null;" json:"UdacityUserKey"`
	Status         string   `gorm:"size:255;not null;" json:"Status"`
	EnrolledAt     time.Time `gorm:"not null;" json:"EnrolledAt,omitempty"`
}

type EnrollmentSave struct {
	NanodegreeKey  string   `json:"NanodegreeKey"`
	UdacityUserKey string   `json:"UdacityUserKey"`
	Status         string   `json:"Status"`
}

func (e *Enrollment) Validate() error {
	if (e.NanodegreeKey=="") {
		return errors.New("Required NanodegreeKey")
	}
	if (e.UdacityUserKey=="") {
		return errors.New("Required UdacityUserKey")
	}
	if (e.Status != "ENROLLED") && (e.Status != "UNENROLLED") {
		return errors.New("Enrollment  status either ‘ENROLLED’ or ‘UNENROLLED’")
	}
	return nil
}

func (e *Enrollment) SaveEnrollment(db *gorm.DB) (*Enrollment, error) {
	e.EnrolledAt = time.Now()
	err := db.Create(&e).Error
	if err != nil {
		return &Enrollment{}, err
	}
	return e, nil
}

func (e *Enrollment) FindEnrollmentByID(db *gorm.DB, uid uint64) (*Enrollment, error) {
	err := db.Model(Enrollment{}).Where("id = ?", uid).Take(&e).Error
	if err != nil {
		return &Enrollment{}, err
	}
	if gorm.IsRecordNotFoundError(err) {
		return &Enrollment{}, errors.New("Enrollment Not Found")
	}
	return e, err
}

func (e *Enrollment) FindAllEnrollments(db *gorm.DB) (*[] Enrollment, error){
	enrollments := []Enrollment{}
	err := db.Model(&Enrollment{}).Find(&enrollments).Error
	if err != nil {
		return &[]Enrollment{}, err
	}
	return &enrollments, err
}

func (e *Enrollment) UpdateAEnrollment(db *gorm.DB, uid uint64) (*Enrollment, error) {

	db = db.Model(&Enrollment{}).Where("id = ?", uid).Take(&Enrollment{}).UpdateColumns(
		map[string]interface{}{
			"NanodegreeKey":  e.NanodegreeKey,
			"UdacityUserKey": e.UdacityUserKey,
			"Status":         e.Status,
		},
	)
	if db.Error != nil {
		return &Enrollment{}, db.Error
	}
	err := db.Model(&Enrollment{}).Where("id = ?", uid).Take(&e).Error
	if err != nil {
		return &Enrollment{}, err
	}
	return e, nil
}

func (e *Enrollment) DeleteEnrollment(db *gorm.DB, uid uint64) (int64, error) {

	db = db.Model(&Enrollment{}).Where("id = ?", uid).Take(&Enrollment{}).Delete(&Enrollment{})

	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
