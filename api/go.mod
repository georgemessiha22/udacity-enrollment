module gitlab.com/georgemessiha22/api

go 1.13

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/go-openapi/spec v0.19.5 // indirect
	github.com/go-openapi/swag v0.19.6 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/jinzhu/gorm v1.9.11
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.1.11
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/swaggo/echo-swagger v0.0.0-20191205130555-62f81ea88919
	github.com/swaggo/swag v1.6.3
	github.com/urfave/cli v1.22.2 // indirect
	golang.org/x/crypto v0.0.0-20191219195013-becbf705a915 // indirect
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/tools v0.0.0-20191220234730-f13409bbebaf // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1
)
