package main

import (
	"os"
	"gitlab.com/georgemessiha22/api/controllers"
)

func main() {
	PORT := os.Getenv("PORT")
	DB_HOST := os.Getenv("DB_HOST")
	DB_USER := os.Getenv("DB_USER")
	DB_PASSWORD := os.Getenv("DB_PASSWORD")
	DB_NAME := os.Getenv("DB_NAME")
	DB_PORT := os.Getenv("DB_PORT")

	server := new(controllers.Server)
	server.Initialize(DB_USER, DB_PASSWORD, DB_PORT, DB_HOST, DB_NAME)
	server.Run(PORT)
}
