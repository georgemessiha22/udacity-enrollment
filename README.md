# Udacity Enrollment Microservice

## Inroduction
This task is to develop Udacity enrollment microservice example using golang, postgreSQL, react, docker and docker-compose.

This task being done in one week, wihtout any perior knowledege of golang or react with previous experience in docker and docker-compose

to run this project use the following command:
```
docker-compose up 
```

##  Description
- /enrollment endpoint create an enrollment for a student. The
enrollment should be composed of the following data.

| key              | Type     | Description |
|:----------------:|:--------:|:--------------------------------------------------------------------------------------------:|
| ID               | STRING   | ID represents the enrollment object, you should auto-generate it on creating new enrollment. |
| nanodegree_key   | STRING   | Key represents nanodegree in Udacity Catalog. |
| udacity_user_key | STRING   | Key represents a user, save it as a dummy key for now for simplicity. |
| enrolled_at      | DATETIME | Date of enrollment creation. |
| status           | STRING   | Enrollment  status either ‘ENROLLED’ or ‘UNENROLLED’. |

- The enrollment service implemented with Golang.
- PostgreSQL as my datastore.
- web app that consumes /enrollments endpoint. It is one page which has a header and a list of Nanodegree cards, each card have an “Enroll” button which enrolls the user in this Nanodegree.
- Fetching the list of available Nanodegrees you need to make a GET request to: https://catalog-api.udacity.com/v1/degrees And display the Nanodegree only if the attribute “available” = true and “open_for_enrollment” = true.
- To create the web app, feel free to use React.
- server side rendering to build the web app. And without any UI libraries.
- models to be implemented in api/models
- controllers hold the server, router, middlewares, objects controllers (modelsName_controller.go)
- make NanodegreeKey, UdacityUserKey unique together to prevent resubmitting of same enrollment

### Enhancement
- add filters to API GET "/v1/enrollments/" so can be used to which courses user already sumbitted
- frontend ENROLL button switch the word between ENROLL and UNENROLL 

## URLS
- to access swagger url: 127.0.0.1:8080/swagger/index.html

## Tweaks
- if you editted in the swagger documentations in the comment of api/controllers/enrollment_controller
  run the below command to regenerate the sagger files
  ```
  docker-compose run web "swag init -g controllers/router.go
  ```